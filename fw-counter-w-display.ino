#include <LiquidCrystal.h>

// object declarations
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

// global vars
volatile uint32_t last = 0; // stores the time of last pulse
volatile uint32_t counter = 0; // counting var for interrupt
uint32_t prevcnt = 0;  // for display refresh, stores the last count value

void setup() {
  // begin LCD and serial port
  Serial.begin(115200);
  lcd.begin(16, 2);

  // prepare external interrupt
  attachInterrupt(digitalPinToInterrupt(3), callback_counter_1, FALLING);

  // clear display and start counter
  lcd.clear();
  lcd.print(prevcnt);
}

void loop() {
  // local counter var
  uint32_t lcounter;

  // disable interrupts!
  cli();

  // get a local copy of the interrupt counter
  lcounter = counter;

  // check for ? character in serial port
  if (Serial.available()) {
    char c = Serial.read();
    if (c == '?') {
      // clear counter on ISR
      counter = 0;
      // print local counter copy
      Serial.println(lcounter);
    }
  }

  // enable interrupts
  sei();

  // update display
  if (lcounter != prevcnt)
  {
    prevcnt = lcounter;
    lcd.clear();
    lcd.print(lcounter);
  }
}

void callback_counter_1()
{
  // ignore interrupts thar occur too soon
  if (millis() - last >= 500) {
    counter++;
    last = millis();
  }
}

